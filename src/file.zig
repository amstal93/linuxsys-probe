const std = @import("std");
const fs = std.fs;
const mem = std.mem;

const Allocator = mem.Allocator;
const File = fs.File;
const Str = []const u8;

pub fn read(alloc: *Allocator, path: Str) ?[:0]const u8 {
    var f = fs.openFileAbsolute(path, .{}) catch {
        return null;
    };
    defer f.close();

    var buffer: [4096]u8 = undefined;
    const bytes_read = f.readAll(&buffer) catch 0;

    return if (bytes_read > 0) mem: {
        const value = mem.trimRight(u8, buffer[0..bytes_read], "\r\n");
        break :mem alloc.dupeZ(u8, value) catch null;
    } else null;
}

pub fn exists(path: Str) bool {
    if (fs.accessAbsolute(path, .{})) |_| {
        return true;
    } else |_| {
        return false;
    }
}
