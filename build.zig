const std = @import("std");

const Builder = std.build.Builder;
const ChildProcess = std.ChildProcess;
const CrossTarget = std.zig.CrossTarget;
const InstallDir = std.build.InstallDir;
const LibExeObjStep = std.build.LibExeObjStep;
const Step = std.build.Step;
const Str = []const u8;

const dist_dir = "target/dist";

const TarStep = struct {
    const Self = @This();

    pub const base_id = .Custom;

    step: Step,
    builder: *Builder,
    artifact: *LibExeObjStep,
    version: ?Str,

    pub fn init(b: *Builder, artifact: *LibExeObjStep, version: ?Str) Self {
        var step = Step.init(
            .Custom,
            b.fmt("{s} {s}", .{ @typeName(Self), artifact.name }),
            b.allocator,
            make,
        );
        step.dependOn(&artifact.step);
        return Self{
            .builder = b,
            .step = step,
            .artifact = artifact,
            .version = version,
        };
    }

    fn make(step: *Step) !void {
        const self = @fieldParentPtr(Self, "step", step);
        const full_path = self.builder.pathFromRoot(dist_dir);
        try self.builder.makePath(full_path);
        const executable_path = self.artifact.installed_path orelse
            self.artifact.getOutputPath();
        const exe = std.fs.path.basename(executable_path);
        const exe_dir = std.fs.path.dirname(executable_path).?;
        const cpu_arch = @tagName(self.artifact.target.cpu_arch.?);
        var tar_name = self.builder.fmt(
            "{s}/{s}-{s}.tar.xz",
            .{ full_path, cpu_arch, exe },
        );
        if (self.version) |version| {
            tar_name = self.builder.fmt(
                "{s}/{s}-{s}-{s}.tar.xz",
                .{ full_path, cpu_arch, exe, self.version },
            );
        }

        const cmd = [_]Str{ "tar", "cvfJ", tar_name, "-C", exe_dir, exe };
        _ = try ChildProcess.exec(
            .{ .allocator = self.builder.allocator, .argv = cmd[0..] },
        );
    }
};

fn addExe(b: *Builder, target: CrossTarget) !*LibExeObjStep {
    const exe = b.addExecutable("linuxsys-probe", "src/main.zig");
    exe.linkLibC();
    exe.setTarget(target);
    exe.override_dest_dir = .{ .Custom = try target.linuxTriple(b.allocator) };
    exe.strip = true;
    exe.setBuildMode(std.builtin.Mode.ReleaseSmall);
    exe.install();
    return exe;
}

fn addTest(b: *Builder, target: CrossTarget) !*Step {
    const test_exe = b.addTest("src/main.zig");
    test_exe.linkLibC();
    test_exe.setTarget(target);
    test_exe.enable_qemu = true;

    const triple = try target.linuxTriple(b.allocator);
    const name = b.fmt("test-{s}", .{triple});
    const title = b.fmt("Test {s} triple", .{triple});
    const test_step = b.step(name, title);
    test_step.dependOn(&test_exe.step);
    return test_step;
}

fn createTar(b: *Builder, artifact: *LibExeObjStep, version: ?Str) !*Step {
    const tar = try b.allocator.create(TarStep);
    tar.* = TarStep.init(b, artifact, version);
    return &tar.step;
}

pub fn build(b: *Builder) !void {
    const clean = b.option(bool, "clean", "Clean artifacts before build") orelse false;
    if (clean) {
        try std.fs.cwd().deleteTree("target");
        try std.fs.cwd().deleteTree("zig-out");
    }
    const x86_64_target = CrossTarget{
        .cpu_arch = .x86_64,
        .os_tag = .linux,
        .abi = .musl,
    };
    const aarch64_target = CrossTarget{
        .cpu_arch = .aarch64,
        .os_tag = .linux,
        .abi = .musl,
    };
    const aarch32_target = try CrossTarget.parse(.{
        .arch_os_abi = "arm-linux-musleabi",
        .cpu_features = "generic+v8a",
    });

    const x86_64_exe = try addExe(b, x86_64_target);
    const aarch64_exe = try addExe(b, aarch64_target);
    const aarch32_exe = try addExe(b, aarch32_target);

    const run_cmd = x86_64_exe.run();
    run_cmd.step.dependOn(b.getInstallStep());
    if (b.args) |args| {
        run_cmd.addArgs(args);
    }

    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);

    const unit_test_step = b.step("unit-test", "Run unit-tests");
    unit_test_step.dependOn(try addTest(b, x86_64_target));
    unit_test_step.dependOn(try addTest(b, aarch64_target));
    unit_test_step.dependOn(try addTest(b, aarch32_target));

    const test_step = b.step("test", "Run all tests");
    test_step.dependOn(b.getInstallStep());
    test_step.dependOn(unit_test_step);
    const cmd = [_]Str{"./test.sh"};
    test_step.dependOn(&b.addSystemCommand(cmd[0..]).step);

    const version = b.option([]const u8, "version", "Version of distributable archives");

    const dist_step = b.step("dist", "Create distributable archives");
    dist_step.dependOn(try createTar(b, x86_64_exe, version));
    dist_step.dependOn(try createTar(b, aarch64_exe, version));
    dist_step.dependOn(try createTar(b, aarch32_exe, version));
}
